package org.bitbucket.zune;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.runner.RunWith;
import org.junit.Rule;
import org.junit.Ignore;
import org.junit.runners.Suite;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;


//@RunWith( Parameterized.class )

public class LeapYearTests {
    private Zune z = new Zune("leap");

    @Test
    public void test1() {
        assertEquals(true, z.isLeapYear(1980));
    }

    @Test
    public void test2() {
        assertEquals(true, z.isLeapYear(1988));
    }

    @Test
    public void test3() {
        assertEquals(false, z.isLeapYear(1999));
    }
}
